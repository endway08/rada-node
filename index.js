var http = require('http');
var express = require('express');
var WebSocketServer = require('ws').Server;
var session = require('express-session');
var fs = require('fs');
var jade = require('jade');
var path = require('path');
var cheerio = require('cheerio');
var request = require('request');

var app = express();
var httpServer = http.createServer(app);
var wss = new WebSocketServer({ port: 3000 });

var DeputiesList = require('./modules/DeputiesList.js');
var DraftLawList = require('./modules/DraftLawList.js');

app
	.use('/scripts', express.static(path.join(__dirname, 'scripts')))

	.set('views', './views/admin')
	.set('view engine', 'jade')

	.get('/deputies/', function(request, response) {
		DeputiesList.getList(function (list) {
			response.render('deputy/list', {'data': list});
		});
	})
	.get('/deputies/:id([0-9]+)', function(request, response) {
		DeputiesList.getItem(request.params.id, function (item) {
			response.render('deputy/form', {'form': item});
		});
	})
	.get('/draft-law/', function(request, response) {
		DraftLawList.getList(function (list) {
			response.render('law/list', {'data': list});
		});
	})
	.get('/draft-law/:id([0-9]+)', function(request, response) {
		DraftLawList.getItem(request.params.id, function (item) {
			response.render('law/item', {'form': item});
		});
	});

wss
	.on('connection', function connection(ws) {
	  	// var html = fs.readFileSync(htmlfile).toString();
		ws.send('something');

		ws.on('message', function (e) {
			if (e.data = 'start') {
				DeputiesList.getList(function (list) {
					ws.send(JSON.stringify({
						action: 'DeputiesList',
						data: list
					}));
				});
				// console.log('start chtoli')
				// request('http://itd.rada.gov.ua/mps/info/page/16067', function  (err, res, html) {
				// 	if (!err) {					
				// 		var $ = cheerio.load(html);

				// 		ws.send(JSON.stringify({
				// 			action: 'set',
				// 			html: $('#mp_content').html()
				// 		}));
				// 	}
				// })
			}
		})

		// ws.on('message', function (data) {
		// 	var response = {};

		// 	switch (data.action) {
		// 		case "deputy_form":
		// 			response.html = app.render('deputy_form', {first_name: 'Oleg', last_name: 'Lyashko'});
		// 			response.action = 'render';
		// 			break;

		// 	}

		// 	if (response.action)
		// 		ws.send(response);
		// })
	});

var port = process.env.PORT || 8090;
httpServer.listen(port, function() {
	console.log("Listening on " + port);
});