// DraftLawList

var fs = require('fs');
var md5 = require('MD5');
var xml2js = require('xml2js').parseString;
var request = require('request');
var iconv = require('iconv-lite');

var url = 'http://w1.c1.rada.gov.ua/pls/zweb2/pkg_rss.new_zp';
var listDataFile = ['./data/', md5(url) + (+new Date() / 3600 * 1000), '.db'].join('');

function win1251_to_utf8(str) {
	return iconv.decode(str, 'win1251');
}

var DraftLawList = {
	getItem: function (index, callback) {
		DraftLawList.getList(function (list) {
			callback && callback(list[index]);
		});
	},
	getList: function (callback) {
		var html;

		try {
			html = fs.readFileSync(listDataFile);
			// , function (err, html) {
			// 	callback(DraftLawList.parseXml(html));
			// });
		} catch(err) {
			if (err.code === 'ENOENT') {
				request({
					url: url,
					method: 'GET',
					encoding: 'binary',
				}, function  (err, res, html) {
					if (!err) {
						callback && callback(DraftLawList.parseXml(win1251_to_utf8(html)));

						fs.open(listDataFile, 'w+', function (err, fd) {
							fs.write(fd, html);
						})
					}
				})
			}
		}
	},
	parseXml: function (xml) {
		var list = [];

		xml2js(xml, {trim: true}, function (err, result) {
			if (!err) {
				if (result.rss) {
					for (index in result.rss.channel[0].item) {
						var item = result.rss.channel[0].item[index];

						list.push({
							name: item.title[0],
							link: item.link[0],
							desc: item.description[0],
							date: item.pubDate[0]
						})
					}
				}
			}
		});

		return list;
	}
};

module.exports = DraftLawList;