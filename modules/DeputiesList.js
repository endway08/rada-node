// DeputiesList

var fs = require('fs');
var md5 = require('MD5');
var cheerio = require('cheerio');
var request = require('request');
var iconv = require('iconv-lite');

var url = 'http://w1.c1.rada.gov.ua/pls/site2/fetch_mps?skl_id=9';
var listDataFile = ['./data/', md5(url), '.db'].join('');

function win1251_to_utf8(str) {
	return iconv.decode(str, 'win1251');
}

var DeputiesList = {
	getItem: function (index, callback) {
		DeputiesList.getList(function (list) {
			callback && callback(list[index]);
		});
	},
	getList: function (callback) {
		var html;

		try {
			fs.readFile(listDataFile, function (err, html) {
				callback(DeputiesList.parseHtml(html));
			});
		} catch(err) {
			if (err.code === 'ENOENT') {
				request({
					url: url,
					method: 'GET',
					encoding: 'binary',
				}, function  (err, res, html) {
					if (!err) {
						callback && callback(DeputiesList.parseHtml(win1251_to_utf8(html)));

						fs.open(listDataFile, 'w+', function (err, fd) {
							fs.write(fd, html);
						})
					}
				})
			}
		}
	},
	parseHtml: function (html) {
		var deputies = [],
			$ = cheerio;

		$.load(html)('ul.search-filter-results > li').each(function (index, el) {
			deputies.push({
				name: win1251_to_utf8($(this).find('.title a').text()),
				link: $(this).find('.title a').attr('href'),
				img: $(this).find('.thumbnail img').attr('src')
			});
		});

		return deputies;
	}
};

module.exports = DeputiesList;