var exampleSocket = new WebSocket("ws://localhost:3000/", "protocolOne");
exampleSocket.onmessage = function(event) {
    var data = {};
    if (event.data && event.data.substr(0,1) == '{') {
        data = JSON.parse(event.data);
    } else {
        data.action = event.data;
    }

    if (data.action == 'something') {
        exampleSocket.send('start', function (rspns) {
            console.log(rspns)
        });
    } else if (data.action == 'DeputiesList') {
        var html = '<ul>';
        for (i in data.data) {
            html += ['<li><a href="', data.data[i].link, '">', data.data[i].name, '</a></li>'].join(''); 
        }
        document.querySelector('body div').innerHTML = html;
    }
};
